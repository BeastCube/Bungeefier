package com.vexsoftware.votifier.model;

public abstract interface VoteListener {
    public abstract void voteMade(Vote paramVote);
}
