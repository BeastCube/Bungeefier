package com.vexsoftware.votifier.model;

import com.vexsoftware.votifier.Votifier;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class ListenerLoader {

    public static List<VoteListener> load(String directory) {
        List listeners = new ArrayList();
        File dir = new File(directory);

        if (!dir.exists()) {
            Votifier.getInstance().getLogger().log(Level.WARNING, "No listeners loaded! Cannot find listener directory '" + dir + "' ");

            return listeners;
        }
        ClassLoader loader;
        try {
            loader = new URLClassLoader(new URL[]{dir.toURI().toURL()}, VoteListener.class.getClassLoader());
        } catch (MalformedURLException ex) {
            Votifier.getInstance().getLogger().log(Level.SEVERE, "Error while configuring listener class loader", ex);

            return listeners;
        }
        File[] arrayOfFile;
        int j = (arrayOfFile = dir.listFiles()).length;
        for (int i = 0; i < j; i++) {
            File file = arrayOfFile[i];
            if (file.getName().endsWith(".class")) {
                String name = file.getName().substring(0, file.getName().lastIndexOf("."));
                try {
                    Class clazz = loader.loadClass(name);
                    Object object = clazz.newInstance();
                    if (!(object instanceof VoteListener)) {
                        Votifier.getInstance().getLogger().info("Not a vote listener: " + clazz.getSimpleName());
                    } else {
                        VoteListener listener = (VoteListener) object;
                        listeners.add(listener);
                        Votifier.getInstance().getLogger().info("Loaded vote listener: " + listener.getClass().getSimpleName());
                    }

                } catch (Exception ex) {
                    Votifier.getInstance().getLogger().log(Level.WARNING, "Error loading '" + name + "' listener! Listener disabled.");
                } catch (Error ex) {
                    Votifier.getInstance().getLogger().log(Level.WARNING, "Error loading '" + name + "' listener! Listener disabled.");
                }
            }
        }
        return listeners;
    }
}
