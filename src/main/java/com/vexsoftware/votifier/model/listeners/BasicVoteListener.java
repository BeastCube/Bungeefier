package com.vexsoftware.votifier.model.listeners;

import com.vexsoftware.votifier.Votifier;
import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VoteListener;

public class BasicVoteListener implements VoteListener {

    public void voteMade(Vote vote) {
        Votifier.getInstance().getLogger().info("Received: " + vote);
    }
}
