package com.vexsoftware.votifier;

import com.vexsoftware.votifier.crypto.RSAIO;
import com.vexsoftware.votifier.crypto.RSAKeygen;
import com.vexsoftware.votifier.model.ListenerLoader;
import com.vexsoftware.votifier.model.VoteListener;
import com.vexsoftware.votifier.net.VoteReceiver;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginDescription;

import java.io.*;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Votifier extends Plugin {
    private static Votifier instance;
    private String version;
    private final List<VoteListener> listeners = new ArrayList<>();
    private VoteReceiver voteReceiver;
    private KeyPair keyPair;
    private boolean debug;
    String host;
    int port;

    public void onEnable() {
        instance = this;
        this.version = getDescription().getVersion();

        File rsaDirectory = new File(getDataFolder() + "/rsa");

        String listenerDirectory = getDataFolder().toString().replace("\\", "/") + "/listeners";

        File dir = new File("plugins/Bungeefier/");
        if (!dir.exists())
            dir.mkdirs();
        File rsadir = new File("plugins/Bungeefier/rsa/");
        if (!rsadir.exists()) {
            rsadir.mkdirs();
            File pub = new File("plugins/Bungeefier/rsa/public.key");
            try {
                pub.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            File pri = new File("plugins/Bungeefier/rsa/private.key");
            try {
                pri.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        File config = new File("plugins/Bungeefier/config.txt");

        if (!config.exists()) {
            try {
                getLogger().info("Configuring Votifier for the first time...");
                config.createNewFile();
                PrintWriter writer = new PrintWriter(new FileWriter(config, true));
                writer.println("0.0.0.0");
                writer.println("8124");
                writer.println("false");
                writer.close();
                getLogger().info("------------------------------------------------------------------------------");
                getLogger().info("Assigning Votifier to listen on port 8192. If you are hosting Craftbukkit on a");
                getLogger().info("shared server please check with your hosting provider to verify that this port");
                getLogger().info("is available for your use. Chances are that your hosting provider will assign");
                getLogger().info("a different port, which you need to specify in config.yml");
                getLogger().info("------------------------------------------------------------------------------");
            } catch (Exception ex) {
                getLogger().log(Level.SEVERE, "Error creating configuration file", ex);
                gracefulExit();
                return;
            }
        }
        try {
            if (!rsaDirectory.exists()) {
                rsaDirectory.mkdir();
                new File(listenerDirectory).mkdir();
                this.keyPair = RSAKeygen.generate(2048);
                RSAIO.save(rsaDirectory, this.keyPair);
            } else {
                this.keyPair = RSAIO.load(rsaDirectory);
            }
        } catch (Exception ex) {
            getLogger().log(Level.SEVERE, "Error reading configuration file or RSA keys", ex);

            gracefulExit();
            return;
        }

        listenerDirectory = "plugins/Votifier/listeners";
        this.listeners.addAll(ListenerLoader.load(listenerDirectory));


        Scanner sc = null;
        try {
            sc = new Scanner(config);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        this.host = sc.nextLine();
        this.port = Integer.parseInt(sc.nextLine());
        this.debug = Boolean.parseBoolean(sc.nextLine());
        sc.close();


        if (this.debug) {
            getLogger().info("DEBUG mode enabled!");
        }
        final String host = this.host;
        final int port = this.port;
        try {
            FutureTask<VoteReceiver> task = new FutureTask<>(() -> {
                //set the classloader to the one BungeeCord used to load Bungeefier with.
                ClassLoader previous = Thread.currentThread().getContextClassLoader();
                Thread.currentThread().setContextClassLoader(Votifier.class.getClassLoader());

                VoteReceiver voteReceiver1 = new VoteReceiver(Votifier.getInstance(), host, port);

                // Reset classloader and return the pool
                Thread.currentThread().setContextClassLoader(previous);
                return voteReceiver1;
            });
            ProxyServer.getInstance().getScheduler().runAsync(Votifier.getInstance(), task);

            try {
                this.voteReceiver = task.get();
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException("Unable to create Redis pool", e);
            }
            this.voteReceiver.setIsPropagator(false);
            this.voteReceiver.setCanReceivePropagation(false);

            this.voteReceiver.start();

            getLogger().info("Votifier enabled.");
        } catch (Exception ex) {
            gracefulExit();
            return;
        }
    }

    public void onDisable() {
        if (this.voteReceiver != null) {
            this.voteReceiver.shutdown();
        }
        getLogger().info("Votifier disabled.");
    }

    private void gracefulExit() {
        getLogger().log(Level.SEVERE, "Votifier did not initialize properly!");
    }

    public static Votifier getInstance() {
        return instance;
    }

    public String getVersion() {
        return this.version;
    }

    public List<VoteListener> getListeners() {
        return this.listeners;
    }

    public VoteReceiver getVoteReceiver() {
        return this.voteReceiver;
    }

    public KeyPair getKeyPair() {
        return this.keyPair;
    }

    public boolean isDebug() {
        return this.debug;
    }

}
