package com.vexsoftware.votifier.crypto;

import com.vexsoftware.votifier.Votifier;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.RSAKeyGenParameterSpec;

public class RSAKeygen {

    public static KeyPair generate(int bits) throws Exception {
        Votifier.getInstance().getLogger().info("Votifier is generating an RSA key pair...");
        KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
        RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(bits, RSAKeyGenParameterSpec.F4);

        keygen.initialize(spec);
        return keygen.generateKeyPair();
    }
}
