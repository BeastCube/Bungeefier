package com.vexsoftware.votifier.net;

import com.lichcraft.propagator.PropagationReader;
import com.lichcraft.propagator.Propagator;
import com.vexsoftware.votifier.Votifier;
import com.vexsoftware.votifier.crypto.RSA;
import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VoteListener;
import net.md_5.bungee.api.ProxyServer;

import javax.crypto.BadPaddingException;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;

public class VoteReceiver extends Thread {
    private final Votifier plugin;
    private final String host;
    private final int port;
    private ServerSocket server;
    private boolean running = true;

    private Propagator propagator = null;
    private boolean canReceivePropagation;

    public VoteReceiver(Votifier plugin, String host, int port) throws Exception {
        this.plugin = plugin;
        this.host = host;
        this.port = port;
        initialize();
    }

    public boolean getIsPropagator() {
        return this.propagator != null;
    }

    public void setIsPropagator(boolean value) {
        if ((value) && (this.propagator == null)) {
            this.propagator = new Propagator();
        } else if ((!value) && (this.propagator != null)) {
            this.propagator = null;
        }
    }


    public boolean getCanReceivePropagation() {
        return this.canReceivePropagation;
    }

    public void setCanReceivePropagation(boolean value) {
        this.canReceivePropagation = value;
    }

    private void initialize() throws Exception {
        try {
            this.server = new ServerSocket();
            this.server.bind(new java.net.InetSocketAddress(this.host, this.port));
        } catch (Exception ex) {
            Votifier.getInstance().getLogger().log(Level.SEVERE, "Error initializing vote receiver. Please verify that the configured IP address and port are not already in use. This is a common problem with hosting services and, if so, you should check with your hosting provider.", ex);

            throw new Exception(ex);
        }
    }

    public void shutdown() {
        this.running = false;

        if (this.server == null)
            return;
        try {
            this.server.close();
        } catch (Exception ex) {
            Votifier.getInstance().getLogger().log(Level.WARNING, "Unable to shut down vote receiver cleanly.");
        }
    }

    public void run() {
        while (this.running) {
            try {
                Socket socket = this.server.accept();
                socket.setSoTimeout(5000);
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

                InputStream in = socket.getInputStream();

                writer.write("VOTIFIER " + Votifier.getInstance().getVersion());
                writer.newLine();
                writer.flush();

                byte[] block = new byte['Ā'];
                in.read(block, 0, block.length);

                String serviceName = "";
                String username = "";
                String address = "";
                String timeStamp = "";

                boolean isPropagated = false;

                if ((this.canReceivePropagation) && (PropagationReader.isPropagationData(block))) {
                    isPropagated = true;

                    PropagationReader reader = new PropagationReader(block);

                    serviceName = reader.strings[1];
                    username = reader.strings[2];
                    address = reader.strings[3];
                    timeStamp = reader.strings[4];
                } else {
                    block = RSA.decrypt(block, Votifier.getInstance().getKeyPair().getPrivate());

                    int position = 0;

                    String opcode = readString(block, position);
                    position += opcode.length() + 1;
                    if (!opcode.equals("VOTE")) {
                        throw new Exception("Unable to decode RSA");
                    }

                    serviceName = readString(block, position);
                    position += serviceName.length() + 1;
                    username = readString(block, position);
                    position += username.length() + 1;
                    address = readString(block, position);
                    position += address.length() + 1;
                    timeStamp = readString(block, position);
                    position += timeStamp.length() + 1;
                }

                final Vote vote = new Vote();
                vote.setServiceName(serviceName);
                vote.setUsername(username);
                vote.setAddress(address);
                vote.setTimeStamp(timeStamp);

                if (this.plugin.isDebug()) {
                    if (isPropagated) {
                        Votifier.getInstance().getLogger().info("Received vote record [propagated]-> " + vote);
                    } else {
                        Votifier.getInstance().getLogger().info("Received vote record -> " + vote);
                    }
                }
                for (VoteListener listener : Votifier.getInstance().getListeners()) {
                    try {
                        listener.voteMade(vote);
                    } catch (Exception ex) {
                        String vlName = listener.getClass().getSimpleName();
                        Votifier.getInstance().getLogger().log(Level.WARNING, "Exception caught while sending the vote notification to the '" + vlName + "' listener", ex);
                    }
                }


                this.plugin.getProxy().getScheduler().runAsync(this.plugin, new Runnable() {
                    public void run() {
                        System.out.println("BungeeVote received! Calling event!");
                        ProxyServer.getInstance().getPluginManager().callEvent(new com.vexsoftware.votifier.model.VotifierEvent(vote));
                    }
                });
                writer.close();
                in.close();
                socket.close();

                if (this.propagator != null) {
                    this.propagator.disperse(serviceName, username, address, timeStamp);
                }
            } catch (SocketException ex) {
                Votifier.getInstance().getLogger().log(Level.WARNING, "Protocol error. Ignoring packet - " + ex.getLocalizedMessage());
            } catch (BadPaddingException ex) {
                Votifier.getInstance().getLogger().log(Level.WARNING, "Unable to decrypt vote record. Make sure that that your public key");

                Votifier.getInstance().getLogger().log(Level.WARNING, "matches the one you gave the server list.", ex);
            } catch (Exception ex) {
                Votifier.getInstance().getLogger().log(Level.WARNING, "Exception caught while receiving a vote notification", ex);
            }
        }
    }

    private String readString(byte[] data, int offset) {
        StringBuilder builder = new StringBuilder();
        for (int i = offset; (i < data.length) &&
                (data[i] != 10); ) {
            builder.append((char) data[i]);

            i++;
        }

        return builder.toString();
    }
}
