package com.lichcraft.propagator;

import com.vexsoftware.votifier.Votifier;

import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class PropagationTarget extends Thread {
    private String hostname = "";
    private int port = 0;
    private ArrayList<byte[]> queue = null;
    private AutoResetEvent asr = null;
    private boolean serverMarkedBad = false;

    public boolean isRunning = false;

    public PropagationTarget(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
        this.queue = new ArrayList();
        this.asr = new AutoResetEvent(false);

        start();
    }

    public String getHostname() {
        return this.hostname;
    }

    public int getPort() {
        return this.port;
    }

    public void shutdown() {
        this.isRunning = false;
        try {
            this.asr.set();
        } catch (Exception localException) {
        }
        Votifier.getInstance().getLogger().warning("<MECH> Failed to shutdown propagation target");
    }

    public void enqueue(byte[] b) {
        synchronized (this.queue) {
            this.queue.add(b);

            if (!this.serverMarkedBad) this.asr.set();
        }
    }

    private void enqueueRepulse() {
        Timer timer = new Timer();
        timer.schedule(new RepulseTask(this), 30000L);
    }

    public void run() {
        Votifier.getInstance().getLogger().info("<MECH> Votifier target '" + getHostname() + ":" + getPort() + "' is running");

        this.isRunning = true;

        while (this.isRunning) {
            try {
                byte[] data = null;

                synchronized (this.queue) {
                    if ((this.queue.size() != 0) && (!this.serverMarkedBad)) {
                        data = (byte[]) this.queue.get(0);
                        this.queue.remove(0);
                    }
                }

                if (data == null) {
                    this.asr.waitOne();
                } else {
                    Socket socket = new Socket();
                    try {
                        socket.connect(new InetSocketAddress(getHostname(), getPort()));
                        OutputStream socketOutputStream = socket.getOutputStream();
                        socketOutputStream.write(data);
                    } catch (Exception ex) {
                        Votifier.getInstance().getLogger().info("<MECH> Failed to connect to votifier @ '" + getHostname() + "." + getPort() + "' :: " + ex.getMessage());
                        synchronized (this.queue) {
                            this.serverMarkedBad = true;
                            this.queue.add(data);
                            enqueueRepulse();
                        }

                    }
                }
            } catch (Exception ex) {
                Votifier.getInstance().getLogger().warning("<MECH> Fatal error while processing propagation target '" + getHostname() + "'  shutting down this target." + ex.getMessage());
                this.isRunning = false;
            }
        }
        Votifier.getInstance().getLogger().info("<MECH> Votifier target '" + getHostname() + ":" + getPort() + "' has shutdown");
    }

    private String readString(byte[] data, int offset) {
        StringBuilder builder = new StringBuilder();
        for (int i = offset; (i < data.length) &&
                (data[i] != 10); ) {
            builder.append((char) data[i]);

            i++;
        }

        return builder.toString();
    }

    private class RepulseTask extends TimerTask {
        private PropagationTarget owner;

        public RepulseTask(PropagationTarget owner) {
            this.owner = owner;
        }

        public void run() {
            Votifier.getInstance().getLogger().info("<MECH> Retrying votifier " + PropagationTarget.this.getHostname() + "." + PropagationTarget.this.getPort());
            this.owner.serverMarkedBad = false;
            this.owner.asr.set();
        }
    }
}
