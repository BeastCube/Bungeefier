package com.lichcraft.propagator;

import com.vexsoftware.votifier.Votifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StringWriter;
import java.util.ArrayList;

public class Propagator {
    private ArrayList<PropagationTarget> targets = null;

    public Propagator() {
        this.targets = new ArrayList();

        Votifier.getInstance().getLogger().info("<MECH> Data path is " + Votifier.getInstance().getDataFolder());

        File invFile = new File(Votifier.getInstance().getDataFolder(), "propagate.mech");

        if (!invFile.exists()) {
            Votifier.getInstance().getLogger().info("<MECH> propagation configuraton not found");
        } else {
            try {
                BufferedReader br = new BufferedReader(new FileReader(invFile));
                String line;
                while ((line = br.readLine()) != null) {
                    if (line.equals("")) {
                        int colon = line.indexOf(':');

                        if (colon == -1) {
                            Votifier.getInstance().getLogger().warning("<MECH> Address format must be <host>:<port>");
                        }
                        String host = line.substring(0, colon);
                        try {
                            int port = Integer.parseInt(line.substring(colon + 1));

                            PropagationTarget target = new PropagationTarget(host, port);

                            this.targets.add(target);

                            Votifier.getInstance().getLogger().info("<MECH> propagation server: " + line);
                        } catch (Exception ex) {
                            Votifier.getInstance().getLogger().warning("<MECH> Address format must be <host>:<port>");
                        }
                    }
                }

                br.close();
            } catch (Exception localException1) {
            }
        }
    }


    public void disperse(String serviceName, String username, String address, String timeStamp) {
        StringWriter out = new StringWriter();

        out.write("MECH:VOTIFIER:PROPAGATION");
        out.write(0);

        out.write(serviceName);
        out.write(0);

        out.write(username);
        out.write(0);

        out.write(address);
        out.write(0);

        out.write(timeStamp);
        out.write(0);
        try {
            for (PropagationTarget target : this.targets) {
                if (target.isRunning) {
                    target.enqueue(out.toString().getBytes("UTF-8"));
                }
            }
        } catch (Exception localException) {
        }
    }

    public void shutdown() {
        if (!this.targets.isEmpty()) {
            for (PropagationTarget target : this.targets) {
                target.shutdown();
            }
        }
    }
}
