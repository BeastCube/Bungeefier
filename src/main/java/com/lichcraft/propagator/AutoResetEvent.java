package com.lichcraft.propagator;

class AutoResetEvent {
    private final Object monitor = new Object();
    private volatile boolean open = false;

    public AutoResetEvent(boolean open) {
        this.open = open;
    }

    public void waitOne() throws InterruptedException {
        synchronized (this.monitor) {
            while (!this.open) {
                this.monitor.wait();
            }
            this.open = false;
        }
    }

    public void set() {
        synchronized (this.monitor) {
            this.open = true;
            this.monitor.notify();
        }
    }

    public void reset() {
        this.open = false;
    }
}
