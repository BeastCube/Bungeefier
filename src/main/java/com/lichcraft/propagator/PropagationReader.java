package com.lichcraft.propagator;

public class PropagationReader {
    private byte[] data;
    private int pointer;
    public String[] strings;

    public PropagationReader(byte[] data) {
        this.strings = new String[5];
        int p = 0;

        StringBuilder b = new StringBuilder();
        try {
            byte[] arrayOfByte;
            int j = (arrayOfByte = data).length;
            for (int i = 0; i < j; i++) {
                byte c = arrayOfByte[i];
                if (c == 0) {
                    this.strings[p] = b.toString();
                    b = new StringBuilder();
                    p++;
                } else {
                    b.append((char) c);
                }
            }
        } catch (Exception localException) {
        }
    }

    public static boolean isPropagationData(byte[] data) {
        PropagationReader pr = new PropagationReader(data);

        return pr.strings[0].compareToIgnoreCase("MECH:VOTIFIER:PROPAGATION") == 0;
    }
}
